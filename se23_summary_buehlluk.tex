% SE SUMMARY FS 23

\documentclass[a4paper, landscape, 8pt]{extarticle}

\usepackage{pgf-umlcd}
\usepackage{pgf-umlsd}

\usepackage[utf8]{inputenc} % for utf-8

\usepackage{lukasbuehler_eth_summary_style}

\setlength{\headheight}{0pt}

\usepackage{fancyhdr}
\pagestyle{fancy}
\fancyhf{}
\lhead{Summary: Software Engineering $\cdot$ FS 2023 $\cdot$ Dr. Felix Friedrich Wicker, Dr. Malte Schwerhoff, Dr. Hermann Lehner}
\rhead{Lukas Bühler (Bachelor CSE) $\quad$ \thepage / \pageref{LastPage}}

% Remove section numbers
\setcounter{secnumdepth}{0}


\begin{document}
\begin{multicols*}{4}
    \thispagestyle{fancy}

    Main Activities of Software Development:
    \begin{enumerate}
        \item Requirements Elicitation (Requirements Specification)
        \item Design
              \begin{itemize}
                  \item System Design: Software architecture, Components (Computational units and their interface: Filters, databases, layers), Connectors (Interactions between components)
                  \item Detailed Design: Choosing how to implement: Data structures, algorithms and subclass hierarchies
              \end{itemize}
        \item Implementation
        \item Validation
    \end{enumerate}

    \section{Requirements Elicitation}
    Elicitation := The process of getting or producing something, especially information or a reaction
    \subsection{Requirements}
    Requirements Specification: Describe the user's view (What not how)

    \begin{definition}[Requirement]
        A feature that the system must have or a constraint it must satisfy to be accepted by the client.
    \end{definition}

    Requirements Engineering: Defines the requirements of the system under construction.

    Results in document: Requirements Specification

    \begin{definition}[Functional Requirement]
        Describes relationship of outputs to inputs
        \begin{itemize}
            \item Functionality: What is the software supposed to do?
            \item External Interfaces: Interaction with people, hard- and software. (Source and type of input and output)
        \end{itemize}
    \end{definition}

    \begin{definition}[Nonfunction Requirements]
        All other requirements that are not functional requirements.
        \begin{itemize}
            \item Performance: Speed, availability, response time, recovery time
            \item Attributes (Quality requirements): Portability, correctness, maintainability, security
            \item Design Constraints: Required standards, operating environment, etc.
        \end{itemize}
    \end{definition}

    Requirements Validation: A quality assurance step, usually after requirements elicitation or analysis

    \subsection{Requirements Elicitation Activities}
    The following things have to be identified:
    \begin{enumerate}
        \item Actors: Represent roles (What kind of user, external system or the physical environment)
        \item Scenarios: Describe behaviour from users POV. Mostly common cases, focus on understandability
        \item Use Cases: Generalizes scenarios to describe all possible cases. Focus on completeness
        \item Nonfunctional requirements
    \end{enumerate}

    \begin{definition}[Scenario]
        A narrative description of what people do and experience as they try to make use of computer
        systems and applications.

        It has different applications during the software lifecycle:
        \begin{itemize}
            \item Requirements Elicitation
            \item Client Acceptance Testing
            \item System Deployment
        \end{itemize}
    \end{definition}

    \begin{definition}[Use Case]
        A list of steps describing the interaction between an actor and the system to achieve a goal.

        A use case consists of
        \begin{itemize}
            \item Unique name
            \item Initiating and participating actors
            \item Flow of events
            \item Entry conditions
            \item Exit conditions
            \item Exceptions
            \item Special requirements
        \end{itemize}
    \end{definition}

    \vspace*{\fill}
    \columnbreak
    \section{Modeling and Specification}


    \subsection{Code Documentation}

    Design choices must be communicated to the developers.
    Code alone is insuficient to document design decisions.

    \subsubsection{Code Documentation: For clients}

    How to use the code: Document the interface

    \begin{itemize}
        \item Constructors \& public member functions:
              \begin{itemize}
                  \item How to call: Explicit parameters (properties of arguments), implicit state (of this object)
                  \item Returned result
                  \item Effects on program state: Reciever effects, heap effects, control-flow effects
                  \item Performance
                  \item Invariants (class properties, example: list is sorted, ...)
                  \item Histroy invariants (evolution, example: a list is immutable)
              \end{itemize}
        \item Supertypes
        \item External functions, e.g. \texttt{<<} or \texttt{std::hash} (Maybe)
    \end{itemize}


    \subsubsection{Code Documentation: For implementers}

    How does the code work: Document the implementation

    \begin{itemize}
        \item Internal code comments regarding evolution
        \item Justification for decisions
    \end{itemize}


    \subsubsection{Code Documentation: How to document}

    \begin{itemize}
        \item Code comments
        \item Annotations (\texttt{@})
        \item Types and modifiers
        \item Assertions
        \item Contracts (stylized assertion)
    \end{itemize}



    \subsection{Informal Models}

    Unified Modeling Language `UML' uses text and graphical notation to document specification, analysis,
    design and implementation.

    \begin{itemize}
        \item Use case diagrams: requirements of a system
        \item class diagrams: structure of a system
        \item Interaction diagrams: message passing
              \begin{itemize}
                  \item Sequence diagrams
                  \item Collaboration diagrams
              \end{itemize}
        \item State and activity diagrams: actions of an object
        \item Implementation diagram
              \begin{itemize}
                  \item Component model: dependencies between code
                  \item Deployment model: strucure of the runtime system
              \end{itemize}
        \item Object constraint language (OCL)?
    \end{itemize}

    \subsubsection{Static Models}

    Classes and Instances (Objects)
    \begin{center}
        \resizebox{4cm}{!}{%
            \begin{tikzpicture}
                \begin{class}[text width=4.5cm]{ClassName}{0,0}
                    \attribute{attribute : type}
                    \operation{operation() : string}
                \end{class}

                \begin{object}[shift={(0,-1)}]{instaceName:ClassName}{ClassName}
                    \attribute{attribute = data}
                \end{object}
            \end{tikzpicture}%
        }
    \end{center}

    Associations and Multiplicity
    \begin{center}
        \resizebox{\columnwidth}{!}{%
            \begin{tikzpicture}
                \begin{class}[text width=2cm]{Person}{0,0}
                \end{class}

                \begin{class}[text width=2cm]{Company}{6,0}
                \end{class}

                \association{Company}{0..*}{employer}{Person}{1..*}{employee}
            \end{tikzpicture}%
        }
    \end{center}

    Navigability
    \begin{center}
        \resizebox{\columnwidth}{!}{%
            \begin{tikzpicture}
                \begin{class}[text width=2cm]{Person}{0,0}
                \end{class}

                \begin{class}[text width=2cm]{Company}{6,0}
                \end{class}

                \draw [->] (Company) -- node[above] {employs} (Person);
            \end{tikzpicture}%
        }
    \end{center}

    Composition (part-of ``has-a'')
    \begin{center}
        \resizebox{\columnwidth}{!}{%
            \begin{tikzpicture}
                \begin{class}[text width=2cm]{Circle}{0,0}
                \end{class}

                \begin{class}[text width=2cm]{Point}{4,0}
                \end{class}

                \begin{class}[text width=2cm]{Polygon}{8,0}
                \end{class}

                \composition{Polygon}{points}{3..*}{Point}
                \composition{Circle}{center}{1}{Point}
            \end{tikzpicture}%
        }
    \end{center}


    Aggragation: (parts that cannot exist independently)
    \begin{center}
        \resizebox{4cm}{!}{%
            \begin{tikzpicture}
                \begin{class}[text width=2cm]{Bat}{0,0}
                \end{class}

                \begin{class}[text width=2cm]{Batwing}{4,0}
                \end{class}

                \aggregation{Bat}{}{}{Batwing}
            \end{tikzpicture}%
        }
    \end{center}

    Generalization and Specialization (kind-of, ``is-a'')
    \begin{center}
        \resizebox{4cm}{!}{%
            \begin{tikzpicture}
                \begin{class}[text width=2cm]{Polygon}{0,0}
                \end{class}

                \begin{class}[text width=2cm]{Rectangle}{4,0}
                    \inherit{Polygon}
                \end{class}
            \end{tikzpicture}%
        }
    \end{center}

    \subsubsection{Dynamic Models}
    Describe the behaviour of a system

    \paragraph*{Sequence Diagrams} Example

    % \begin{center}
    %     \resizebox{\columnwidth}{!}{%                
    %     \begin{sequencediagram}[]
    %         \newinst{user}{User} % our user actor
    %         \newinst{solver}{solver: Solver}
    %         \newinst{formula}{formula: Formula}
    %         \newinst{logger}{logger: Logger}

    %         \begin{call}{user}{solve(formula)}{solver}{}
    %             \begin{call}{solver}{is\_transcendental()}{formula}{tcd}
    %             \end{call}


    %             \begin{sdblock}{alt}{[tcd]}
    %                 \begin{call}{solver}{report\_feature\_error()}{logger}{}
    %                 \end{call}

    %             \end{sdblock}

    %         \end{call}


    %     \end{sequencediagram}
    % }
    % \end{center}

    \includegraphics[width=\columnwidth]{sequence_diagram.png}

    \paragraph*{State Diagrams} Example

    \includegraphics*[width=\columnwidth]{state_diagram.png}

    \begin{itemize}
        \item Activity: Operation performed as long as object is in some state
        \item Event: Something that happens at a point in time
        \item Action: Operation in response to an event
    \end{itemize}

    \paragraph{Model Driven Development (MDD)} Models to code (automatic)

    Advantages:
    \begin{itemize}
        \item Many implementation platforms supported
        \item Less recurring activities for programmers
        \item Uniform code, enforcing coding confentions
        \item Models are not just documentation anymore
    \end{itemize}

    Problems:
    \begin{itemize}
        \item Multiple inheritance, informal/inclimplete specification
        \item Modification of code requires complicated synchronization
        \item Choice of containers not unique
        \item Various choices for asynchronous messages
    \end{itemize}

    \paragraph*{Contracts} We need a way of expressing constraints more formally, so that we can reliably
    and automatically check them.

    For UML, use OCL (Object Contstraint Language). We used Alloy.

    \vspace*{\fill}
    \columnbreak
    \section{Modularity}
    \begin{definition}[Modularity]
        Decomposition achieves modularity. Modularity is an architectural/structural property and is desirable
        on all levels of a system.
    \end{definition}

    \subsection{Coupling}
    \begin{definition}[Coupling]
        Measures interdependence between different modueles. High coupling prevents modularity.
    \end{definition}

    \begin{definition}[Cohesion]
        Degree to which the elements inside a module belong together. Inversly proporional to coupling.
    \end{definition}

    Three approaches are discussed:
    \begin{center}
        \resizebox{\columnwidth}{!}{%
            \begin{tikzpicture}
                \draw[<->,thick] (0,0) -- (6,0);
                \node[align=center] at (0.5,-0.6) {immutable;\\full sharing};
                \node[align=center] at (3,-0.6) {careful,\\fine-grained\\control};
                \node[align=center] at (5.5,-0.6) {mutable;\\no sharing};
            \end{tikzpicture}
        }
    \end{center}

    \begin{definition}[Design pattern]
        General, reusable solution to commonly occurng design problem.
    \end{definition}

    \begin{definition}[Idiom]
        Language-specific way of implementing certain task.
    \end{definition}

    \subsubsection{Data Coupling}

    \begin{definition}[Representation Exposure]
        Modules that expose their internal representation become tightly couples to their clients.
    \end{definition}

    Problems with tight data coupling:
    \begin{itemize}
        \item Changing data representation (maintenance, evolution) affects clients
        \item Clients could rely on unintended, instable properties that stem from technical details.
        \item Cannot maintain strong invariants.
        \item Data consistency may require synchronization
    \end{itemize}

    \paragraph*{Facade Pattern} Restricting and simplifying access (Fine-grained control)

    Provide a single, simplified interface to the more general facilities of a complex module.


    \paragraph*{Flyweight Pattern} Making shared data immutable (Immutable, full sharing)

    Provide a factory that creates copies of immutable flyweight classes.


    \paragraph*{Pipes and Filters Pattern} Avoiding shared data (Mutable, no sharing)

    Data flow is the only form of communication between components and is processed incrementally
    as it arrives. The output usually begins before all input is consumed. Filters must be
    independent of each other.

    \subsubsection{Procedural Coupling}

    Modules depend on other modules whose functions they call any change in callees may require changes
    in the caller.


    \paragraph*{Code refactoring} to reduce the degree of procedural coupling

    Code refactoring is the process of restructuring existing code without changing
    external behaviour. Improve the design, structure, and/or implementation of software while
    preserving its functionality.


    \paragraph*{Restricting Calls} Enforce restrictions for which other modules a module may call.

    Recall also the Facade pattern or most prominently: Multilayered/Multitier architectures:
    A layer depends only on lower layers, has no knowledge of higher layers. Example: MVC.

    \paragraph*{Event-based Communication} Use indirect, dynamic coupling between event generators and
    responders. Example: Observer pattern.

    \paragraph*{Model-View-Controller (MVC)} Popular architectural pattern

    Loose coupling via events but loose control (what, how components will respond)
    \begin{itemize}
        \item Model: core functionality and data
        \item Views: Display information to the user
        \item Controllers: Handle user input
    \end{itemize}

    \includegraphics*[width=\columnwidth]{mvc.png}

    \subsubsection{Class Coupling}

    Classes depend on other classes via types and inheritance.

    \paragraph*{(Abstract) Factory pattern} is a classical object-oriented pattern for abstracting over
    object creation. (Together with inheritance) it abstracts away completely from the
    particularities and dependencies of a class.

    \subsection{Adaptation}
    \begin{definition}[Adaptation]
        Property that measures how adaptable a system is. Important for achieving modularity and prevents
        coupling. It makes inevitable changes easier and facilitates reuse.
    \end{definition}

    \subsubsection{Parameterization}

    Modules can be prepared for change by allowing clients to influence their behaviour. Make modules
    parametric in: values, types, data structures, algorithms.

    \paragraph*{Strategy Pattern} is a classical object-oriented pattern for abstracting over algorithms
    and behaviour. Same can be achieved with static metaprogramming (templates, macros), or with
    higher-order functions (lambdas. closures)

    \subsubsection{Specialization}

    Allows clients to customize behaviour by adding subclasses and overriding methods. In
    object-oriented programs, behaviors can be specialized via overriding and dynamic method binding.

    \paragraph*{Dynamic Method Binding} is a case distinction on the dynamic type of receiver object.

    Drawbacks:
    \begin{itemize}
        \item Reasoning: Subclasses share responsibility for maintaining invariants.
        \item Testing: Dynamic binding increases the number of possible behaviors that need to be
              tested.
        \item Versioning: Dynamic binding makes it harder to evolve code without breaking
              subclasses.
        \item Performance: Overhead of method look-up at runtime.
    \end{itemize}

    \paragraph*{Visitor pattern} separates the algorithm from the object structure. The visitor allows
    adding new virtual functions to a family of classes, without modifying the classes.

    \vspace*{\fill}
    \columnbreak

    \section{Testing}

    \begin{definition}[Testing]
        Testing is the process of executing a program to find deviations from expected behavior (requirements).
    \end{definition}

    \begin{definition}[Test Harness]
        \includegraphics[width=\columnwidth]{test_harness.png}
        \begin{itemize}
            \item `Test Driver': Applies test cases to UUT. They are typically
                  confusingly also referred to as test cases
            \item Unit Under Test (UUT): The unit (part) of software that is tested.
            \item `Test Stub': Simple implementation of a component of the UUT, that
                  simulates the real component.
        \end{itemize}
    \end{definition}

    \subsection{Test Stages}

    \begin{tabular}{l l}
        Requirement Elicitation & System Test      \\
        System Design           & Integration Test \\
        Detailed Design         & Unit Test        \\
    \end{tabular}

    \begin{definition}[Unit Test]
        Tests individual subsystems (function, classes, groups of classes)

        Goal: Confirm the subsytem is correctly implemented and implements the
        required functionality.
    \end{definition}

    \begin{definition}[Test Oracle]
        Evaluates to true or false inside a test stub. Multiple test oracles
        determine whether a test case succeeds.
    \end{definition}

    \paragraph*{Parameterized Unit Tests}
    Using test data as parameters and parametric test oracles, one can
    decouble the test driver from test data. Avoid boilerplate code

    Approaches to generalizing test oracles:
    \begin{enumerate}
        \item with input data and expected output data
        \item asserting relevant properties in output data.
        \item with master solution (solution must be unique)
    \end{enumerate}

    \subsubsection{Integration Testing}

    Testing groups of subsystems and eventually the entire system with the
    goal to test the interfaces between subsystems.


    \subsubsection{System testing}

    Stages of system testing:

    \begin{tabular}{l l}
        Functional Requirements     & Functional Test   \\
        Non-functional Requirements & Performance Test  \\
        Client's requirements       & Acceptance Test   \\
        User Environment            & Installation Test
    \end{tabular}

    \paragraph*{Functional Testing} (Performed by the developer)

    Goal: Test the functionality of the system.

    The test cases are designed from requirements specification and based
    on use cases.


    \paragraph*{Acceptance Testing} (Performed by the client)

    Goal: Demonstrate that the system meets customer requirements and is
    ready to use.

    \begin{itemize}
        \item Alpha test: At developers site, w/ developer
        \item Beta test: At client's site, w/o developer
    \end{itemize}

    \paragraph*{Independent Testing} Testing is done by ``test engineers''.


    \subsection{Test Strategies}

    \includegraphics[width=\columnwidth]{test_strategies.png}

    \subsubsection{Exhaustive Testing}

    Check UUT for all possible inputs. Not feasable.

    \subsubsection{Random Testing}

    Focus is on generating test data fully automatically.
    Avoids designer/tester bias, but treats all inputs as equally valuable and
    is hard to reproduce.

    \subsubsection{Functional Testing}
    Also known as ``black-box testing''.

    Use requirements knowledge to determine test cases, don't derive tests from
    code or design.

    Goal: Cover all requirements. Focus on input/output behaviour.

    But does not reveal errors in specification and not effective for detecting
    coding errors.

    \begin{definition}[Semantic equivalence class]
        Each possible value belongs to one equivalence class.
    \end{definition}

    \begin{definition}[Boundary testing]
        A large number of errors occur at domain boundaries.
        Select values at the edge of each equivalence class (in addition to non-boundary values)
    \end{definition}

    \begin{enumerate}
        \item Select promising values
              \begin{enumerate}
                  \item Paritioning

                        Partition values into semantic equivalence classes by using
                        domain knowledge.

                  \item Selection

                        Select concrete values for testing, for each equivalence
                        class.

                        Use boundary testing to systematically select promising values.
                        

              \end{enumerate}

        \item Create concrete test inputs

              Combine individual values to concrete test inputs. Ideally reduce
              the number of test cases to make the effort feasible with
              combinatorial testing.
    \end{enumerate}

    \paragraph*{Pairwiese Combinations Testing} Instead of testing all possible
    combinations of all inputs, focus on all possible combinations of each
    pair of inputs. (This can be generalized to $k$-way testing)

    \begin{example}[Functional Testing]
    // TODO insert exam example for functional testing
    \end{example}

    \subsubsection{Structural Testing}

    Use design knowledge about system structure, algorithms and data structures
    to determine test cases that exercise a large portion of the code.

    Goal: Achieve high test coverage, explore all execution paths.

    But it is time consuming, implementation specific tests are more likely to
    become obsolete and it requires knowledge about internals which testers
    and clients shouldn't need to have. (Not well suited for system tests)

    \paragraph*{Control-flow Testing} ...


    \begin{definition}[Control-flow graphs (CFGs)]
        Typical internal representations in code analysis tools. 
        
        \begin{center}
            \includegraphics*[width=3cm]{control_flow_graph.png}
        \end{center}
    \end{definition}

    Coverage is a good way of measuring the adequacy of tests.

    \begin{definition}[Statement Coverage]
        What percentage of the total statements does a single test case execute (cover)?
    \end{definition}

    \begin{definition}[Branch Coverage]
        What percentage of all possible branches (edges with conditions) does a test case execute?
        Branch coverage leads to more thorough testing than statement coverage.
        Complete branch coverage implies complete statement coverage, but n\% 
        branch coverage does not generally imply n\% statement coverage.
    \end{definition}

    Goal: Cover all possible branch combinations (paths through the CFG)

    \begin{definition}[Path Coverage]
        The percentage of all the number of executed paths over the total number of paths.

        Path as in a path through the CFG.
        
        Path coverage leads to more thorough testing then both statement and
        branch coverage.

        Complete path coverage is (in general) not feasable for loops.
    \end{definition}

    \begin{definition}[Loop Coverage]
        The percentage of the number of executed loops with 0, 1 or more iterations
        over the total number of loops times 3.

        \[
        \text{Loop Cov.} = \frac{\# \text{Exec. loops (w/ 0, 1, ... iters.)}}{\# \text{Total Loops} \cdot 3}
        \]
    \end{definition}

    Statement and branch coverage are the standard.


    \vspace*{\fill}
    \columnbreak

    \section{Formal Methods}

    \subsection{Concolic Execution}

    \begin{definition}[Symbolic Execution]
        Idea: Compute the symbolic constraints per path, then solve them to get
        concrete inputs that explore all paths.

        \begin{itemize}
            \item Symbolic store $\sigma$: maps variables to symbolic expressions, is updtated by assignment statements.
            \item Path conditions: $\pi$
            \item Symbolic state: Symbolic store + conditions per program point
        \end{itemize}

        Algorithm:
        \begin{enumerate}
            \item Start with an initial symbolic state
            \item Propagate state across all statements (symbolically execute the program)
            \item Duplicate the current state at branch points, one copy per branch
            \item When following a branch, add the corresponding condition to the state
            \item Eventually, we'll have a final state per path
            \item Per final state, solve the path conditions to obtain test inputs that exercise the path.
        \end{enumerate}

        Symbolic execution gets stuck on external libraries, but they could be 
        abstracted away with pre- and post-conditions.
    \end{definition}

    \begin{definition}[Concolic Execution]
        Hybrid Approach that combines testing and proving (via symbolic execution).
        Concrete execution drives the symbolic execution. 
        Ir cannot prove the absence of bugs. 

        It can produce new inputs and test cases to explore unexplored paths.

        May not cover all paths.
    \end{definition}


    \vspace*{\fill}
    \columnbreak

    \subsection{Alloy}

    Alloy is a formal modeling language based on set theory. An alloy model
    specifies a collection of constraints that describe a set of structures.

    \subsubsection{Signatures and Atoms}
    Signatures express a new type.
    \begin{minted}{Alloy}
sig Car {}
    \end{minted}
    Models have elements of signatures, called atoms. They are instances of a signature.

    \subsubsection{Subtypes}

    Signatures can be subtypes of other signatures. They automatically inherit
    all of their parents fields and also can define their own fields.

    \paragraph*{in} Defines an inclusive subtype. Any parent atoms may or may
    not also be of this subtype.

    \begin{minted}{Alloy}
sig BrokenCar in Car {}
sig ParkedCar in Car {}
    \end{minted}

    \paragraph*{extends} Defines a subtype where any parent atom can only match
    up with one extension.

    \begin{minted}{Alloy}
sig Sedan extends Car {}
sig Pickup extends Car {}
    \end{minted}

    \paragraph*{abstract} States that all atoms of this signature have to belong
    to extensions and that there will be no atoms of this type.


    \subsubsection{Sets}

    \subsubsection{Fields (Relations)}

    Inside the signature body one can add relations or fields.
    Relations show how signatures are connected to each other.
    The body of a signature is a list o relations, separated by commas.

    \subsubsection{Expressions}

    \subsubsection{Facts, Predicates and Functions}

    Facts (\texttt{fact}) are constraints that always hold for the model.

    Predicates (\texttt{pred}) are named, parameterized formulas.

    \subsubsection{Dynamic Behaviour}

    \subsubsection{Consitency and Validity}

    \subsubsection{Examples}


\end{multicols*}
\end{document}
